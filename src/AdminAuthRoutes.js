import React from "react";
import { Route } from "react-router-dom";
import Hoc from "./hoc/hoc";
import * as actions from "./store/actions/auth";
import { connect } from "react-redux";

import Login from "./containers/Login";
import Signup from "./containers/Signup";
import CustomLayout from './containers/Layout';
import Profile from './containers/Profile';
import AssignmentList from './containers/AssignmentList';
import AssignmentDetail from './containers/AssignmentDetail';
import AssignmentCreate from './containers/AssignmentCreate';

class AdminAuthRoutes extends React.Component {
  render() {
    return (
      <Hoc>
        <CustomLayout {...this.props}>
          <Route exact path="/admin/login" component={Login} />
          <Route exact path="/admin/signup" component={Signup} />
          <Route exact path="/admin/profile/:id" component={Profile} />
          <Route exact path="/admin/assignment-create" component={AssignmentCreate} />
          <Route exact path="/admin/assignment-list" component={AssignmentList} />
          <Route exact path="/admin/assignments/:id" component={AssignmentDetail} />
        </CustomLayout>

      </Hoc>
    )
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminAuthRoutes);

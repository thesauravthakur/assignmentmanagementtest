import axios from "axios";
import * as actionTypes from "./actionTypes";

const getGradeASNTListStart = () => {
    return {
        type: actionTypes.GET_GREDED_ASSIGNMENT_DETAIL_START
    };
};

const getGradeASNTListSuccess = assignments => {
    return {
        type: actionTypes.GET_GREDED_ASSIGNMENTS_DETAIL_SUCCESS,
        assignments
    };
};

const getGradeASNTListFail = error => {
    return {
        type: actionTypes.GET_GREDED_ASSIGNMENTS_DETAIL_FAIL,
        error: error
    };
};




export const getGradedASNTS = (username, token) => {
    return dispatch => {
        dispatch(getGradeASNTListStart())
        axios.defaults.headers = {
            "Content-Type": "application/json",
            Authorization: `Token ${token}`
        };
        axios
            .get(`http://127.0.0.1:8000/graded-assignments/?username=${username}`)
            .then(res => {
                console.log(res.data)
                const assignment = res.data;
                dispatch(getGradeASNTListSuccess(assignment))
            })
            .catch(err => {
                dispatch(getGradeASNTListFail(err))
            })

    }
}


export const createGrededASNT = (token, assignment) => {
    return dispatch => {
        // dispatch(getASNTCreteStart())
        axios.defaults.headers = {
            "Content-Type": "application/json",
            Authorization: `Token ${token}`
        };
        axios.xsrfCookieName = 'csrftoken'
        axios
            .post(`http://127.0.0.1:8000/graded-assignments/create/`, assignment)
            .then(res => {
                console.log(" submit success")
            })
            .catch(err => {
                // dispatch(getASNTCreteFail())
            })

    }
}


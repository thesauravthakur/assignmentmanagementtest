import axios from 'axios';

export const authAxios = axios.create({
    baseURL: `http://127.0.0.1:8000/`,
    headers: {
        'Content-Type': 'application/json',
        Authorization() {
            return `JWT  ${localStorage.getItem('token')}`
        }
    },
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',
    withCredentials: true
})
import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
    assignments: [],
    error: null,
    loading: false
};

const getGradeASNTListStart = (state, action) => {
    return updateObject(state, {
        error: null,
        loading: true
    });
};

const getGradeASNTListSuccess = (state, action) => {
    return updateObject(state, {
        assignments: action.assignments,
        error: null,
        loading: false
    });
};

const getGradeASNTListFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    });
};









const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_GREDED_ASSIGNMENT_DETAIL_START:
            return getGradeASNTListStart(state, action);
        case actionTypes.GET_GREDED_ASSIGNMENTS_DETAIL_SUCCESS:
            return getGradeASNTListSuccess(state, action);
        case actionTypes.GET_GREDED_ASSIGNMENTS_DETAIL_FAIL:
            return getGradeASNTListFail(state, action);
        default:
            return state;
    }
};

export default reducer;
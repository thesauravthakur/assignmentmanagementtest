import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions/assignments';
import { createGrededASNT } from "../store/actions/gradeAssignment";
import Hoc from '../hoc/hoc';
import { Card, Steps, Skeleton, message } from 'antd';
import Questions from './Questions';
import Choices from '../Components/Choices';
const { Step } = Steps;
const cardStyle = {
    marginTop: "20px",
    marginBottom: "20px"
};
class AssignmentDetail extends React.Component {
    state = {
        userAnswers: {}
    }

    componentDidMount() {
        const id = this.props.match.params.id
        if (this.props.token !== undefined && this.props.token !== null) {
            this.props.getASNTS(this.props.token, id);
        }

    }

    componentWillReceiveProps(newProps) {
        const id = this.props.match.params.id
        if (newProps.token !== this.props.token) {
            if (newProps.token !== undefined && newProps.token !== null) {
                this.props.getASNTS(newProps.token, id);
            }
        }
    }

    onChange = (e, qId) => {
        console.log('radio checked', e.target.value);
        const { userAnswers } = this.state;
        userAnswers[qId] = e.target.value
        this.setState({
            userAnswers
        });
    };
    handelSubmit = () => {
        message.success('Processing complete!')
        const { userAnswers } = this.state
        const assignment = {
            username: this.props.username,
            assignmentId: this.props.assignment.id,
            answers: userAnswers
        }
        this.props.createGrededASNT(
            this.props.token,
            assignment
        )
        this.props.history.push("/admin/assignment-list");


    }

    render() {
        const { assignment } = this.props;
        const { userAnswers } = this.state;

        return (
            <Hoc>
                {
                    Object.keys(assignment).length > 0 ?

                        (<Hoc>
                            {
                                this.props.loading ?
                                    (<Skeleton />)
                                    :
                                    (
                                        <Card title={assignment.title} extra={<div><p href="#">{assignment.teacher}</p></div>}>

                                            <Questions
                                                submit={() => this.handelSubmit()}
                                                questions={assignment.questions.map(q => {
                                                    return (
                                                        <Card type="inner"
                                                            style={cardStyle}
                                                            key={q.id}
                                                            title={`${q.order}.${q.question}`
                                                            } >
                                                            <Choices
                                                                choices={q.choices}
                                                                change={this.onChange}
                                                                questionId={q.order}
                                                                userAnswers={userAnswers}
                                                            />
                                                        </Card>
                                                    )

                                                })} />


                                        </Card>
                                    )
                            }



                        </Hoc>) :
                        (null)
                }
            </Hoc>


        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        assignment: state.assignments.currentAssignment,
        loading: state.assignments.loading,
        username: state.auth.username,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getASNTS: (token, id) => dispatch(actions.getASNTDETAIL(token, id)),
        createGrededASNT: (token, assignment) => dispatch(createGrededASNT(token, assignment))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AssignmentDetail);
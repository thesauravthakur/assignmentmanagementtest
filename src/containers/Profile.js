import React from 'react';
import { List, Typography, Divider, Skeleton } from 'antd';
import { connect } from 'react-redux';
import Hoc from '../hoc/hoc';
import Result from '../Components/Result';
import * as actions from '../store/actions/gradeAssignment';

class Profile extends React.Component {
    componentDidMount() {
        if (this.props.token !== undefined && this.props.token !== null) {
            this.props.getASNTS(this.props.username, this.props.token);
        }

    }

    componentWillReceiveProps(newProps) {

        if (newProps.token !== this.props.token) {
            if (newProps.token !== undefined && newProps.token !== null) {
                this.props.getASNTS(newProps.username, newProps.token);
            }
        }
    }

    render() {
        return (
            <Hoc>
                <Divider orientation="left">{this.props.username}</Divider>
                {this.props.loading ?
                    (< Skeleton active />)
                    :
                    (<List
                        size="large"

                        dataSource={this.props.gradedAssignment}
                        renderItem={assignment => <Result key={assignment.id} grade={assignment.grade} />}
                    />
                    )
                }

            </Hoc>

        );
    }
};
const mapStateToProps = state => {
    return {
        token: state.auth.token,
        username: state.auth.username,
        is_student: state.auth.is_student,
        is_teacher: state.auth.is_teacher,
        error: state.auth.error,
        loading: state.gradedAssignment.loading,
        gradedAssignment: state.gradedAssignment.assignments
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getASNTS: (username, token) => dispatch(actions.getGradedASNTS(username, token))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);




import React from 'react';
import Hoc from '../hoc/hoc';
import { Link } from "react-router-dom"
import { connect } from 'react-redux';
import * as actions from '../store/actions/assignments';
import { List, Typography, Divider, Skeleton } from 'antd';


class AssignmentList extends React.Component {
    componentDidMount() {
        console.log(this.props)
        if (this.props.token !== undefined && this.props.token !== null) {
            this.props.getASNTS(this.props.token);
        }

    }

    componentWillReceiveProps(newProps) {
        console.log(this.props)

        if (newProps.token !== this.props.token) {
            if (newProps.token !== undefined && newProps.token !== null) {
                this.props.getASNTS(newProps.token);
            }
        }
    }

    renderItem(item) {
        return (
            <Link to={`/admin/assignments/${item.id}`}>
                <List.Item>{item.title}</List.Item>
            </Link>
        );
    }

    render() {
        const { assignments } = this.props
        console.log(assignments)
        return (
            <Hoc>
                <Divider orientation="center">Assignments</Divider>

                {
                    this.props.loading ?
                        (<Skeleton active />)
                        :
                        (<List
                            size="large"
                            bordered
                            dataSource={this.props.assignments}
                            renderItem={item => this.renderItem(item)}
                        />
                        )
                }

            </Hoc>
        );
    }
}
const mapStateToProps = state => {
    return {
        token: state.auth.token,
        assignments: state.assignments.assignments,
        loading: state.assignments.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getASNTS: token => dispatch(actions.getASNTS(token))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AssignmentList);
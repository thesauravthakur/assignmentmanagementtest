import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';


const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;


class SideBar extends React.Component {

    render() {
        return (
            <Sider className="site-layout-background" width={200}>
                <Menu
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    style={{ height: '100%' }}
                >
                    <SubMenu
                        key="sub1"
                        title={<span><UserOutlined /><Link to="/admin">Dashboard</Link></span>}
                    >
                    </SubMenu>
                    <SubMenu
                        key="sub2"
                        title={<span><UserOutlined /><Link to="/admin/assignment-list">Assignment List</Link></span>}
                    >
                    </SubMenu>

                </Menu>
            </Sider>



        );
    }

}
export default SideBar;
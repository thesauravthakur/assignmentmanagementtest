import React from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import { Link, withRouter } from "react-router-dom";
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import { connect } from "react-redux";
import * as actions from "../store/actions/auth";
import SideBar from './SideBar';
import '../static/Layout.css';



const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class CustomLayout extends React.Component {

  render() {
    // const {id}=this.props.match.params.id
    return (
      <Layout>
        <Header className="header">
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["2"]}
            style={{ lineHeight: "64px" }}
          >
            {this.props.isAuthenticated ? (
              <Menu.Item key="2" onClick={this.props.logout}>
                <Link to="/admin">LogOut</Link>
              </Menu.Item>
            ) : (
                <Menu.Item key="2">
                  <Link to="/admin/login">Login</Link>
                </Menu.Item>
              )}

          </Menu>
        </Header>
        <Content style={{ padding: '0 50px' }}>

          {this.props.isAuthenticated ?
            (
              <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item><Link to="/admin">Home</Link></Breadcrumb.Item>
                <Breadcrumb.Item><Link to={`/admin/profile/${this.props.userId}`}> Profile</Link></Breadcrumb.Item>
                {this.props.is_teacher && (
                  <Breadcrumb.Item ><Link to="/admin/assignment-create"> Create</Link></Breadcrumb.Item>
                )}

              </Breadcrumb>
            )
            :
            (null)
          }

          <Layout className="site-layout-background" style={{}}>

            {
              this.props.isAuthenticated ?
                (<SideBar />)
                :
                (null)
            }

            <Content style={{ padding: '15px 24px', minHeight: 440, backgroundColor: 'white' }}>
              {

                this.props.children

              }
            </Content>
          </Layout>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Admin Dashboard</Footer>
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(actions.logout())
  };
};
const mapStateToProps = state => {
  return {
    userId: state.auth.userId,
    is_teacher: state.auth.is_teacher,
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CustomLayout)
);



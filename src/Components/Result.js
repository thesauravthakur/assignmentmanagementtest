import React from 'react';
import { Progress } from 'antd';
const Result = props => <Progress percent={props.grade} size="small" status="active" />
export default Result;
import React from 'react';
import { Radio } from 'antd';
const radioStyle = {
    display: 'block',
    height: '30px',
    lineHeight: '30px',
};
class Choices extends React.Component {
    state = {
        value: 1,
    };


    render() {

        const { value } = this.state;
        const { questionId } = this.props;
        const { userAnswers } = this.props;
        return (
            <Radio.Group onChange={(e, qId) => this.props.change(e, questionId)} value={userAnswers[questionId] !== null && userAnswers[questionId] !== undefined ?
                userAnswers[questionId] : null
            }>
                {this.props.choices.map((choice, index) => {
                    return (
                        <Radio key={index} style={radioStyle} value={choice}>
                            {choice}
                        </Radio>
                    )
                })}



            </Radio.Group>
        );
    }
}

export default Choices;
from django.apps import AppConfig


class AsassignmentsConfig(AppConfig):
    name = 'asassignments'

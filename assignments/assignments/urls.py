from rest_framework.routers import DefaultRouter
from assignments.views import AssignmentViewSets

router = DefaultRouter()
router.register(r'', AssignmentViewSets, base_name='assignment')
urlpatterns = router.urls

from rest_framework import serializers

from .models import Assignment, Question, User, Choice, GradedAssignment


class StringSerializer(serializers.StringRelatedField):
    def to_internal_value(self, value):
        return value


class QuestionSerializer(serializers.ModelSerializer):
    choices = StringSerializer(many=True)

    class Meta:
        model = Question
        fields = ("id", "choices", "question", "order")


class AssignmentSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()
    teacher = StringSerializer(many=False)

    class Meta:
        model = Assignment
        fields = ("__all__")

    def get_questions(self, obj):
        questions = QuestionSerializer(obj.questions.all(), many=True).data
        #we are able to use (assignment)obj.question.all()  beacause we have used related_name="question" in Question Model
        return questions

    def create(self, request):
        data = request.data
        # print(data)
        assignment = Assignment()
        teacher = User.objects.get(username=data["teacher"])
        assignment.teacher = teacher
        assignment.title = data["title"]
        assignment.save()

        order = 1
        for q in data["questions"]:
            newQ = Question()
            newQ.question = q["title"]
            newQ.order = order
            newQ.save()

            for c in q["choices"]:
                newc = Choice()
                newc.title = c
                newc.save()
                newQ.choices.add(newc)

            newQ.answer = Choice.objects.create(title=q["answer"])
            newQ.assignment = assignment
            newQ.save()
            order += 1
        return assignment


class GrededAssignmentSerializer(serializers.ModelSerializer):
    student = StringSerializer(many=False)

    class Meta:
        model = GradedAssignment
        fields = ("__all__")

    def create(self, request):
        data = request.data
        print(data)
        assignment = Assignment.objects.get(id=data["assignmentId"])
        student = User.objects.get(username=data["username"])
        graded_assignment = GradedAssignment()
        graded_assignment.assignment = assignment
        graded_assignment.student = student
        questions = [q for q in assignment.questions.all()]
        answers = [data['answers'][a] for a in data["answers"]]

        answer_correct = 0
        for i in range(len(questions)):
            if questions[i].answer.title == answers[i]:
                answer_correct += 1
            i += 1
        grade = (answer_correct / len(questions)) * 100
        grade = '%.2f' % grade
        graded_assignment.grade = grade
        graded_assignment.save()
        return graded_assignment

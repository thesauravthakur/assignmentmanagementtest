from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from .serializers import AssignmentSerializer, GrededAssignmentSerializer
from .models import Assignment, GradedAssignment


class AssignmentViewSets(viewsets.ModelViewSet):
    serializer_class = AssignmentSerializer
    queryset = Assignment.objects.all()

    def create(self, request):
        serializer = AssignmentSerializer(data=request.data)
        if serializer.is_valid:
            assignment = serializer.create(request)
            #this is create method is on the serializer
            if assignment:
                return Response(HTTP_200_OK)
        return Response(HTTP_400_BAD_REQUEST)


class GradedAssignmentListView(ListAPIView):
    serializer_class = GrededAssignmentSerializer

    def get_queryset(self):
        queryset = GradedAssignment.objects.all()
        username = self.request.query_params.get("username", None)
        #here query_params is getting the username from the url of the Profile
        if username is not None:
            queryset = queryset.filter(student__username=username)
            #here student__username is the users(student's) username [this is beacuse of the foraginkey
        return queryset


class GradedAssignmentCreateView(CreateAPIView):
    serializer_class = GrededAssignmentSerializer
    queryset = GradedAssignment.objects.all()

    def post(self, request):
        serializer = GrededAssignmentSerializer(data=request.data)
        if serializer.is_valid:
            greded_assignment = serializer.create(request)
            #this is create method is on the serializer
            if greded_assignment:
                return Response(HTTP_200_OK)
        return Response(HTTP_400_BAD_REQUEST)
